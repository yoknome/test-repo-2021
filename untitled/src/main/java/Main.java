import com.github.javafaker.Faker;

import java.util.Locale;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Faker faker = new Faker(new Locale("de"));
        String[] h1 = {"Fußball","Schwimmen","Essen","Schlafen","Tenis","Scheißen"};

        Random r = new Random();

        System.out.println("L E B E N S L A U F\n-------------------");
        System.out.println("\n\n");
        System.out.println("Name:           "+faker.name().name());
        System.out.println("Straße:       "+faker.address().streetAddress());
        System.out.println("Ort:            "+ faker.address().zipCode() + " " + faker.address().city()+"\n");
        System.out.println("Geboren:        "+ faker.backToTheFuture().date() + "\n");
        System.out.println("Hobbys:         "+h1[r.nextInt(h1.length)]);


    }
}
